
<h1> XTABLE </h1>

R-Shiny app associated with the manuscript "Interrogating the precancerous evolution of pathway dysfunction in lung squamous cell carcinoma using XTABLE" by Matthew Roberts [1,2], Julia Ogden [2,3], Mukarram Hossain [1,2], Alastair Kerr [1,2], Jennifer E. Beane [4], Caroline Dive [1,2] and Carlos Lopez-Garcia [2,3]. 

1. Cancer Biomarker Centre, Cancer Research UK Manchester Institute, University of Manchester, United Kingdom

2. Cancer Research UK Lung Cancer Centre of Excellence

3. Translational Lung Cancer Biology Laboratory, Cancer Research UK Manchester Institute, University of Manchester, United Kingdom

4. Boston University School of Medicine, Boston, MA, USA

R can be downloaded from: https://cloud.r-project.org/ <br>
The app has worked in R versions 3.6, 4.0.3, 4.1.0 and 4.2.0. <br>
RStudio can be downloaded from: https://www.rstudio.com/products/rstudio/download/#download

If at any point you're asked if you would like to restart R before installing packages, we found it best to say no, install the packages first and then restart R if necessary once all packages are installed.

Packages are finished installing once you see a message like "the downloaded packages are in...". You may be asked if you want to update all/some/none packages, type "a" and press enter.

To run the app on the R command line, copy and paste the following lines of code. If you've already done these steps and have the packages installed, just skip to the last line of code to download the app again. If you already have the package(s) installed, no message will appear and you can proceed with the next step.

```
if (!requireNamespace("BiocManager", quietly = TRUE)){install.packages("BiocManager")}
```

First install the package that is required to install Bioconductor packages and can be used to install CRAN packages as well. There is  one package that is installed from a different repository:

```
install.packages("estimate", repos="http://r-forge.r-project.org", dependencies=TRUE)
```

Next, install the rest of the packages:

```
BiocManager::install( c("shiny", "magrittr", "dplyr", "ggplot2", "RColorBrewer", "pheatmap", "pROC", "ggpubr", "matrixStats", "imsig", "rstudioapi", "utils", "stats", "tidyr", "tibble", "readr", "GEOquery", "limma",  "ideal", "fgsea", "pathview", "org.Hs.eg.db", "enrichR", "gage", "gageData", "ReactomePA", "edgeR", "progeny", "dorothea", "AnnotationDbi", "Biobase", "parallel", "geneLenDataBase", "TxDb.Hsapiens.UCSC.hg38.knownGene"))
```

You may need to increase the time allowed to download data:

```
options(timeout=600)
```

To download and run the app:

```
shiny::runUrl("https://gitlab.com/cruk-mi/xtable/-/archive/master/xtable-master.tar.gz")
```
