
<h1> Files_Ensembl </h1>

Files required for matching microarray probe IDs (if applicable), Ensembl IDs, gene symbols and Entrez IDs. 

Following files were downloaded from Ensembl BioMart website: https://www.ensembl.org/biomart/martview/ <br>
Database: Ensembl Genes 104; Human Genes GRCh38.p13 <br>
Attributes used to generate files: <br>
Ensembl_BioMart_AgilentWholeGenome_4x44_v1_2021_May_104.txt: 'Gene stable ID' and 'AGILENT WholeGenome 4x44k v1 probe' <br>
Ensembl_BioMart_Transcript_2021_May_104.txt: 'Gene stable ID' and 'Transcript stable ID' <br>
Ensembl_BioMart_GeneName_2021_May_104.txt: 'Gene stable ID' and 'Gene name' <br>
Ensembl_BioMart_EntrezID_2021_May_104.txt: 'Gene stable ID' and 'NCBI gene (formerly Entrezgene) ID' <br>
Ensembl_BioMart_AFFY_HuGene_1_0_st_v1_probe_2021_Dec_104.txt: 'Gene stable ID' and 'AFFY HuGene 1 0 st v1 probe' <br>
