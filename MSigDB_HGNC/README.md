
<h1> MSigDB_HGNC </h1>

Molecular Signature collections using gene symbols (Entrez IDs are another option) retrieved from https://www.gsea-msigdb.org/gsea/msigdb/index.jsp for enrichment analysis using "fgsea" R package. 
