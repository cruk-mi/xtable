
<h1> 0.0.0 </h1>
---

<h3> New features </h3>  

-  None

<h3> Bug fixes   </h3>  

- None

<h3> Issues   </h3>  

-  Multiple samples from same patient  
    -  GSE33479: 122 samples from 77 patients  
    -  GSE109743: 448 samples from 50 patients  
-  Due to above issue, t-test assumption not met (independent observations)  

<h3> Improvements for future releases  </h3>  

-  Resolving the issue of having multiple samples from same patient  
    -  General analyses
        -  Option to keep one sample from same patient  
        -  Option to keep one sample from same patient per grade/stage/molecular-subtype  
    -  DEG Analysis   
        -  Implement random effect  
            -  GSE33479: lme4::lme (convergence error for some genes and with lme can remove these genes )  
            -  GSE109743: limma::duplicateCorrelate
-  Normalised (e.g. ssGSEA/GSVA) signature scores
-  ESTIMATE deconvolution results for GSE109743
-  Update MSigDB collections (currently using v7.1)



