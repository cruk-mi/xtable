
<h1> Files_data </h1>

Files in this directory are required for the app. 

<h2> CIN70genes.csv </h2>

List of chromosome instability genes can be found in the supplementary table 1 from Carter, S. L., Eklund, A. C., Kohane, I. S., Harris, L. N. & Szallasi, Z. A signature of chromosomal instability inferred from gene expression profiles predicts clinical outcome in multiple human cancers. <em>Nat. Genet.</em>, 38(9):1043-1048. doi: 10.1038/ng1861.
The individual genes of the fusion gene C20orf/TGIF2 were used.

<h2> ESTIMATE* </h2>

The R package ESTIMATE was used for deconvolution analysis for microarray data. The file "ESTIMATE_common_genes" is the output of the function 'filterCommonGenes' that was used to check for missing genes in the gene expression dataset. Analysis results for GSE33479, GSE108124 and GSE114489 were saved with prefix "ESTIMATEscore_" and the corresponding GEO accession ID. 

Yoshihara, K., Shahmoradgoli, M., Martínez, E., Vegesna, R., Kim, H., Torres-Garcia, W., Treviño, V., Shen, H., Laird, P. W., Levine, D. A., Carter, S. L., Getz, G., Stemke-Hale, K., Mills, G. B. & Verhaak, R. G. W. Inferring tumour purity and stromal and immune cell admixture from expression data. <em>Nat. Commun.</em>, 4:2612. doi: 10.1038/ncomms3612.

Weblink #1: https://bioinformatics.mdanderson.org/estimate/rpackage.html <br>
Weblink #2: https://bioinformatics.mdanderson.org/public-software/estimate/

<h2> GSE109743* </h2>

Data for GSE109743 was downloaded manually from https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE109743 and saved as "GSE109743_exp_count.txt". The file "GSE109743_metadata.csv" was created manually from https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE109743 from individual sample links and using "Title" and "Characteristics". "GSE109743_Metadata_Table2.csv" is "Source Data" supplementary file "41467_2019_9834_MOESM33_ESM.xlsx" and sheet "Table 2" from Beane, J. E., Mazzilli, S. A., Campbell, J. D., Duclos, G., Krysan, K., Moy, C., Perdomo, C., Schaffer, M., Liu, G., Zhang, S., Liu, H., Vick, J., Dhillon, S. S., Platero, S. J., Dubinett, S. M., Stevenson, C., Reid, M. E., Lenburg, M. E. & Spira, A. E. Molecular subtyping reveals immune alterations associated with progression of bronchial premalignant lesions.<em> Nat. Commun.</em>, 10, 1856. https://doi.org/10.1038/s41467-019-09834-2. 

<h2> gset* </h2>

Files beginning with prefix "gset_" are data files retrieved from GEO using the "GEOquery" package and provided to improve the performance of the app. Code can be found in the script "01_Generate_ProbeMapping_File.R". 
